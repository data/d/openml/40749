# OpenML dataset: data-v3.en-es-lit.clean.anno_without_14top.uniform

https://www.openml.org/d/40749

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

EN-ES-literals for training. The 14 instances in the top are used for validation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40749) of an [OpenML dataset](https://www.openml.org/d/40749). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40749/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40749/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40749/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

